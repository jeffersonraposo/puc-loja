﻿using Microsoft.EntityFrameworkCore;
using Product.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Data
{
	public class ProductContext : DbContext
	{
		public ProductContext(DbContextOptions<ProductContext> options) : base(options)
		{

		}

		public DbSet<Seller> Sellers { get; set; }
		public DbSet<ProductCategory> Categories { get; set; }
		public DbSet<ProductItem> Products { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Seller>().ToTable("Seller");
			modelBuilder.Entity<ProductCategory>().ToTable("ProductCategory");
			modelBuilder.Entity<ProductItem>().ToTable("Product");
		}
	}
}
