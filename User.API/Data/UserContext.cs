﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.API.Data
{
	public class UserContext : IdentityDbContext<Model.User>

	{
		public UserContext(DbContextOptions<UserContext> options) : base(options)
		{
			
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.Model.GetEntityTypes().Select(e => e.Relational()).ToAsyncEnumerable()
				.ForEachAsync(e => e.TableName = e.TableName.Replace("AspNet", ""));
		}
	}
}
