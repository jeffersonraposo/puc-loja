﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageBus
{
	public interface IMessageBus
	{
		void Send(string queue, IIntegrationEvent integrationEvent);
		void Subscribe<T>(string queue, Action<T> action) where T : IIntegrationEvent;
	}
}
