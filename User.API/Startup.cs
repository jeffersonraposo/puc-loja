﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using User.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using User.API.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace User.API
{
	public class Startup
	{
		private readonly IConfiguration _configuration;

		public Startup(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
			services.AddDbContext<UserContext>(opts => opts.UseSqlServer(_configuration["ConnectionString"]));
			services.AddTransient<ILoginService<Model.User>, EFLoginService>();

			services.AddIdentity<Model.User, IdentityRole>(opt => 
			{
				opt.Password.RequiredLength = 3;
				opt.Password.RequireLowercase = false;
				opt.Password.RequireUppercase = false;
				opt.Password.RequireNonAlphanumeric = false;
				opt.Password.RequireDigit = false;
			}).AddEntityFrameworkStores<UserContext>()
			.AddDefaultTokenProviders();

			services.AddJWTAuth(_configuration);
			services.AddSwaggerDoc("User API", "v1");
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				//app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}
			loggerFactory.AddLog4Net();
			app.UseHttpsRedirection();
			app.UseAuthentication();
			app.UseSwagger();
			app.UseSwaggerUI(options =>
			{
				options.SwaggerEndpoint("./swagger/v1/swagger.json", "User API");
				options.RoutePrefix = "";
			});
			app.UseMvc();
		}
	}
}
