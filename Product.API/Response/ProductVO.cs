﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Response
{
	public class ProductVO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public double Price { get; set; }
		public string Picture { get; set; }

		public static ProductVO From(Model.ProductItem product)
		{
			return new ProductVO { Id = product.Id, Name = product.Name, Price = product.Price, Picture = product.Pictures.First()?.Uri };
		}
	}
}
