﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Product.API.Data;
using Product.API.Model;
using Product.API.Response;

namespace Product.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductContext _context;

        public ProductController(ProductContext context)
        {
            _context = context;
        }

        // GET: api/Product
		/// <summary>
		/// List all products
		/// </summary>
		/// <returns></returns>
        [HttpGet]
		[ProducesResponseType(typeof(ProductVO), StatusCodes.Status200OK)]
        public async Task<IEnumerable<ProductVO>> GetProducts([FromQuery]int? category)
        {
			IQueryable<ProductItem> products = _context.Products;
			if (category.HasValue)
			{
				products = products.Where(n => n.Category.Id == category);
			}

			return await products.Include(p => p.Pictures).Select(n => ProductVO.From(n)).ToListAsync();
        }

        // GET: api/Product/5
		/// <summary>
		/// Get product by id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        [HttpGet("{id}")]
		[ProducesResponseType(typeof(ProductItem), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProductItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productItem = await _context.Products.FindAsync(id);
			//falha proposital
			if (id == 999)
			{
				int a = productItem.AvailableStock / 0;
			}

			if (productItem == null)
            {
                return NotFound();
            }

            return Ok(productItem);
        }

        // PUT: api/Product/5
		/// <summary>
		/// Update product
		/// </summary>
		/// <param name="id"></param>
		/// <param name="productItem"></param>
		/// <returns></returns>
        [HttpPut("{id}")]
		[Authorize]
		public async Task<IActionResult> PutProductItem([FromRoute] int id, [FromBody] ProductItem productItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(productItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Product
		/// <summary>
		/// Create new product
		/// </summary>
		/// <param name="productItem"></param>
		/// <returns></returns>
        [HttpPost]
		[Authorize]
		public async Task<IActionResult> PostProductItem([FromBody] ProductItem productItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Products.Add(productItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductItem", new { id = productItem.Id }, productItem);
        }

        // DELETE: api/Product/5
		/// <summary>
		/// Delete product
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        [HttpDelete("{id}")]
		[Authorize]
        public async Task<IActionResult> DeleteProductItem([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productItem = await _context.Products.FindAsync(id);
            if (productItem == null)
            {
                return NotFound();
            }

            _context.Products.Remove(productItem);
            await _context.SaveChangesAsync();

            return Ok(productItem);
        }

        private bool ProductItemExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}