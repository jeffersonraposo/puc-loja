﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Product.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using MessageBus;
using Product.API.Service;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;

namespace Product.API
{
	public class Startup
	{
		private ILogger<Startup> _logger;
		public Startup(IConfiguration configuration, ILogger<Startup> logger)
		{
			_configuration = configuration;
			_logger = logger;
		}

		private readonly IConfiguration _configuration;

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddJsonOptions(opts => opts.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
			services.AddDbContext<ProductContext>(opts => opts.UseSqlServer(_configuration["ConnectionString"]));
			services.AddTransient<IMessageBus, MessageBus.NServiceBus.MessageBus>();
			services.AddTransient<UpdateProductService>();

			services.AddJWTAuth(_configuration);
			services.AddSwaggerDoc("Product API", "v1");
			services.AddCors();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, ProductContext productContext, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				//app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			loggerFactory.AddLog4Net();
			app.UseHttpsRedirection();
			app.UseAuthentication();
			app.UseCors(options => {
				options.AllowAnyOrigin();
				options.AllowAnyHeader();
				options.AllowAnyMethod();
			});
			app.UseSwagger();
			app.UseSwaggerUI(options =>
			{
				options.SwaggerEndpoint("./swagger/v1/swagger.json", "Product API");
				options.RoutePrefix = "";
			});

			app.UseMvc();

			serviceProvider.GetService<UpdateProductService>().Subscribe();

			try
			{
				DbInitializer.Initialize(productContext);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "DbInitializer Error");
			}
		}
	}
}
