import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../shared/services/categories.service';
import { ICategory } from '../shared/models/category.model';

@Component({
  selector: 'app-category-menu',
  templateUrl: './category-menu.component.html',
  styleUrls: ['./category-menu.component.css']
})
export class CategoryMenuComponent implements OnInit {

  categories: ICategory[];
  constructor(private service:CategoriesService) { }

  ngOnInit() {
    this.service.list().subscribe(c => this.categories = c);
  }

}
