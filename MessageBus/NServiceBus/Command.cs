﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessageBus.NServiceBus
{
	class Command : ICommand
	{
		public string Type { get; set; }
		public string Data { get; set; }
	}
}
