import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProduct } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private readonly API = "https://localhost:5003/api/";
  constructor(private http: HttpClient) { }

  list(categoryId) {
    var uri = this.API + 'product';
    uri += categoryId ? `?category=${categoryId}` : '';
    return this.http.get<IProduct[]>(uri);
  }
}
