﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Model
{
	public class ProductItem
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public double Price { get; set; }
		public int AvailableStock { get; set; }
		public ProductCategory Category { get; set; }
		public Seller Seller { get; set; }
		public int ProductSellerId { get; set; }
		public IReadOnlyCollection<Picture> Pictures { get; set; }
	}
}
