import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICategory } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private readonly API = "https://localhost:5003/api/";
  constructor(private http: HttpClient) { }

  list(){
    return this.http.get<ICategory[]>(this.API + 'category');
  }
}
