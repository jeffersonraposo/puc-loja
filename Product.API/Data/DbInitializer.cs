﻿using Product.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Data
{
	public class DbInitializer
	{
		public static void Initialize(ProductContext context)
		{
			if (context.Categories.Any())
			{
				return;
			}

			var sellers = new Seller[] { new Seller { Name = "PUC", Key = Guid.NewGuid().ToString() } };
			context.Sellers.AddRange(sellers);

			var categories = new ProductCategory[] { new ProductCategory { Name = "Smartphones" } };
			context.Categories.AddRange(categories);

			var products = new List<ProductItem> { new ProductItem { Name = "Galaxy S10", Description = "Galaxy S10 - Description",
				AvailableStock = 100, Price = 4399.90F, Category = categories[0], Seller = sellers[0], ProductSellerId = 95 } };
			context.Products.AddRange(products);
			context.SaveChanges();
		}
	}
}
