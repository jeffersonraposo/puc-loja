﻿using Newtonsoft.Json;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace MessageBus.NServiceBus
{
	public class MessageBus : IMessageBus
	{
		private static Dictionary<string, List<object>> s_subscribers = new Dictionary<string, List<object>>();

		public async void Send(string queue, IIntegrationEvent integrationEvent)
		{
			integrationEvent.Created = DateTime.Now;
			var endpointConfiguration = new EndpointConfiguration(queue);
			endpointConfiguration.SendOnly();
			endpointConfiguration.UseSerialization<NewtonsoftSerializer>();
			var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
			transport.ConnectionString("host=localhost");
			transport.UseDirectRoutingTopology();
			var endpoint = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);
			await endpoint.Send(queue, new Command { Type = integrationEvent.GetType().Name, Data = JsonConvert.SerializeObject(integrationEvent) }).ConfigureAwait(false);
		}

		public async void Subscribe<T>(string queue, Action<T> action)
			where T : IIntegrationEvent
		{
			AddSubscriber(action);
			var endpointConfiguration = new EndpointConfiguration(queue);
			endpointConfiguration.UseSerialization<NewtonsoftSerializer>();
			var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
			transport.ConnectionString("host=localhost");
			transport.UseDirectRoutingTopology();
			var endpoint = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);
		}

		public void AddSubscriber<T>(Action<T> action)
		{
			string eventName = typeof(T).Name;
			if (!s_subscribers.ContainsKey(eventName))
			{
				s_subscribers.Add(eventName, new List<object>());
			}

			s_subscribers[eventName].Add(action);
		}

		class MessageHandler : IHandleMessages<Command>
		{
			public Task Handle(Command message, IMessageHandlerContext context)
			{
				MessageBus.s_subscribers.TryGetValue(message.Type, out List<object> handlers);
				if (handlers == null)
				{
					return null;
				}
				var type = handlers.First().GetType();
				var eventType = type.GenericTypeArguments[0];
				var handleMethod = type.GetMethod("Invoke");
				handlers.ForEach(h => handleMethod.Invoke(h, new object[] { JsonConvert.DeserializeObject(message.Data, eventType) }));
				return Task.CompletedTask;
			}
		}
	}
}
