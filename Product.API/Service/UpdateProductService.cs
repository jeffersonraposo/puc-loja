﻿using MessageBus;
using Microsoft.Extensions.DependencyInjection;
using Product.API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Service
{
	public class UpdateProductService
	{
		private readonly IMessageBus _messageBus;
		private readonly IServiceScopeFactory _serviceProvider;

		public UpdateProductService(IMessageBus messageBus, IServiceScopeFactory serviceProvider)
		{
			_messageBus = messageBus;
			_serviceProvider = serviceProvider;
		}

		public void Subscribe()
		{
			_messageBus.Subscribe<ProductUpdated>("pucminas", p => 
			{
				using (var scope = _serviceProvider.CreateScope())
				{
					var context = scope.ServiceProvider.GetService(typeof(ProductContext)) as ProductContext;
					var product = context.Products.SingleOrDefault(x => x.Seller.Key == p.SellerKey && x.ProductSellerId == p.ProductId);
					if (product != null && product.Price != p.Price)
					{
						product.Price = p.Price;
						context.SaveChanges();
					}
				}
			});
		}

		public class ProductUpdated : IntegrationEvent
		{
			public string SellerKey { get; set; }
			public int ProductId { get; set; }
			public double Price { get; set; }
		}
	}
}
