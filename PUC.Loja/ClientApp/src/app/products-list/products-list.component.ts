import { Component, OnInit } from '@angular/core';
import { IProduct } from '../shared/models/product.model';
import { ProductsService } from '../shared/services/products.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  products: IProduct[];
  queryParamSubscription: Subscription;

  constructor(private service: ProductsService, private route: ActivatedRoute) { }

  updateProducts(categoryId) {
    this.service.list(categoryId).subscribe(p => this.products = p);
  }

  ngOnInit() {
    this.queryParamSubscription = this.route.queryParams.subscribe(p => this.updateProducts(p['category']));
  }

  ngOnDestroy() {
    this.queryParamSubscription.unsubscribe();
  }
}
