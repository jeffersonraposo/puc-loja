﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Model
{
	public class ProductCategory
	{
		public int Id { get; set; }
		public string Name { get; set; }
		[JsonIgnore]
		public IReadOnlyCollection<ProductItem> Products { get; set; }
	}
}
