﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MessageBus
{
	public interface IEventHandler<T> where T : IIntegrationEvent
	{
		Task Handle(T integrationEvent);
	}
}
