﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageBus
{
	public interface IIntegrationEvent
	{
		DateTime Created { get; set; }
	}

	public class IntegrationEvent : IIntegrationEvent
	{
		public DateTime Created { get; set; }
	}
}
