﻿using MessageBus;
using NServiceBus.Logging;
using System;

namespace SimulaFornecedor
{
	class Program
	{
		static void Main(string[] args)
		{
			//Desabilitar log no NServiceBus
			NServiceBus.Logging.LogManager.UseFactory(new LoggerFactory());

			Console.WriteLine("=======================================================");
			Console.WriteLine("ALTERAR PRODUTO");
			Console.WriteLine("=======================================================");
			Console.WriteLine("ID: 95\tNome: Galaxy S10");
			Console.Write("Novo preço: ");
			string preco = Console.ReadLine();
			IMessageBus messageBus = new MessageBus.NServiceBus.MessageBus();
			messageBus.Send("pucminas", new ProductUpdated { SellerKey = "8f140946-d492-452b-81c1-e5be02b6d55d", ProductId = 95, Price = Convert.ToDouble(preco) });
			Console.WriteLine("Alterado!");
			Console.ReadLine();
		}

		public class ProductUpdated : IntegrationEvent
		{
			public string SellerKey { get; set; }
			public int ProductId { get; set; }
			public double Price { get; set; }
		}
	}

	class LoggerFactory : ILoggerFactory
	{
		public ILog GetLogger(Type type)
		{
			return new Log();
		}

		public ILog GetLogger(string name)
		{
			return new Log();
		}

		public class Log : ILog
		{
			public bool IsDebugEnabled => false;

			public bool IsInfoEnabled => false;

			public bool IsWarnEnabled => false;

			public bool IsErrorEnabled => false;

			public bool IsFatalEnabled => false;

			public void Debug(string message)
			{
				
			}

			public void Debug(string message, Exception exception)
			{
				
			}

			public void DebugFormat(string format, params object[] args)
			{
				
			}

			public void Error(string message)
			{
				
			}

			public void Error(string message, Exception exception)
			{
				
			}

			public void ErrorFormat(string format, params object[] args)
			{
				
			}

			public void Fatal(string message)
			{
				
			}

			public void Fatal(string message, Exception exception)
			{
				
			}

			public void FatalFormat(string format, params object[] args)
			{
				
			}

			public void Info(string message)
			{
				
			}

			public void Info(string message, Exception exception)
			{
				
			}

			public void InfoFormat(string format, params object[] args)
			{
			}

			public void Warn(string message)
			{
			}

			public void Warn(string message, Exception exception)
			{
			}

			public void WarnFormat(string format, params object[] args)
			{
			}
		}
	}
}
