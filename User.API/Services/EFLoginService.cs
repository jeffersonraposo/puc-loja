﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace User.API.Services
{
	public class EFLoginService : ILoginService<Model.User>
	{
		private readonly UserManager<Model.User> _userManager;
		private readonly SignInManager<Model.User> _signInManager;

		public EFLoginService(UserManager<Model.User> userManager, SignInManager<Model.User> signInManager)
		{
			_userManager = userManager;
			_signInManager = signInManager;
		}

		public async Task<Model.User> FindByUsername(string user)
		{
			return await _userManager.FindByEmailAsync(user);
		}

		public async Task<bool> ValidateCredentials(Model.User user, string password)
		{
			return await _userManager.CheckPasswordAsync(user, password);
		}

		public Task SignIn(Model.User user)
		{
			return _signInManager.SignInAsync(user, true);
		}

		public Task SignInAsync(Model.User user, AuthenticationProperties properties, string authenticationMethod = null)
		{
			return _signInManager.SignInAsync(user, properties, authenticationMethod);
		}
	}
}

