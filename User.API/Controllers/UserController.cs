﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using User.API.Data;
using User.API.Model;
using User.API.Request;
using User.API.Services;

namespace User.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
		private readonly ILoginService<Model.User> _loginService;
		private readonly IConfiguration _configuration;
		private readonly UserManager<Model.User> _userManager;

        public UserController(ILoginService<Model.User> loginService, IConfiguration configuration, UserManager<Model.User> userManager)
        {
			_loginService = loginService;
			_configuration = configuration;
			_userManager = userManager;
        }
		
		/// <summary>
		/// Authenticate user
		/// </summary>
		/// <param name="loginRequest"></param>
		/// <returns></returns>
		[HttpPost("login")]
		[ProducesResponseType(typeof(Response.JwtToken), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status403Forbidden)]
		public async Task<object> Login([FromBody] LoginRequest loginRequest)
		{
			var user = await _loginService.FindByUsername(loginRequest.Email);
			if (await _loginService.ValidateCredentials(user, loginRequest.Password))
			{
				return GenerateJwtToken(loginRequest.Email, user);
			}

			return Forbid();
		}

		/// <summary>
		/// Register new User
		/// </summary>
		/// <param name="registerRequest"></param>
		/// <returns></returns>
		[HttpPost("register")]
		[ProducesResponseType(typeof(Response.JwtToken), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public async Task<object> Register([FromBody] RegisterRequest registerRequest)
		{
			var user = new Model.User
			{
				Name = registerRequest.Name,
				UserName = registerRequest.Email,
				Email = registerRequest.Email
			};
			var result = await _userManager.CreateAsync(user, registerRequest.Password);

			if (result.Succeeded)
			{
				await _loginService.SignInAsync(user, null);
				return GenerateJwtToken(registerRequest.Email, user);
			}
			
			return BadRequest();
		}

		/// <summary>
		/// Get User information
		/// </summary>
		/// <param name="email"></param>
		/// <returns></returns>
		[Authorize]
		[HttpGet("{email}")]
		[ProducesResponseType(typeof(Response.User), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status401Unauthorized)]
		public async Task<object> GetUser([FromRoute] string email)
		{
			if (User?.FindFirstValue(JwtRegisteredClaimNames.Sub) != email)
			{
				return Unauthorized();
			}
			Model.User user = await _userManager.FindByEmailAsync(email);
			return new Response.User { Name = user.Name, Email = user.Email, PhoneNumber = user.PhoneNumber };
		}

		private Response.JwtToken GenerateJwtToken(string email, Model.User user)
		{
			var claims = new List<Claim>
			{
				new Claim(JwtRegisteredClaimNames.Sub, email),
				new Claim(ClaimTypes.Name, user.Name)
			};

			var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
			var expires = DateTime.Now.AddMinutes(Convert.ToDouble(_configuration["JwtExpireMinutes"]));

			var token = new JwtSecurityToken(
				_configuration["JwtIssuer"],
				_configuration["JwtIssuer"],
				claims,
				expires: expires,
				signingCredentials: creds
			);
			return new Response.JwtToken
			{
				Type = "bearer",
				AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
				ExpiresIn = (int)TimeSpan.FromMinutes(Convert.ToDouble(_configuration["JwtExpireMinutes"])).TotalSeconds
			};
		}
    }
}