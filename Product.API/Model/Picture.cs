﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.API.Model
{
	public class Picture
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Uri { get; set; }
	}
}
